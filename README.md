Scribus IRC
===========

A Python plugin to connect Scribus to an IRC channel.

Disclaimer: this has not be tested since 2011, we believe the good file to use is qtbot2.py. It has not been tested with new versions of Scribus. This package is mostly for archiving and analyzing purposes.

Pierre Marchand and Alexandre Leray wrote this in 2011 for a workshop at the Royal College of Art in London where we designed a poster collaboratively at the same time. One computer was beaming a Scribus document, people were sending commands from Scribus API in the chat room. We were even joined remotely by nitrofurano who sent commands blindly (we would send him screenshots periodically).

We then repeated the experience a few months later in a workshop in Saigon, Vietnam.

This was our first attempt at designing collaboratively and synchronously before (html2print)[http://osp.kitchen/tools/html2print/].
And this is from there that the OSP frog got abstracted! :)


